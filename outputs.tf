output "tiller_rolebinding_name" {
  value = kubernetes_cluster_role_binding.tiller.subject[0].name
}

output "tiller_rolebinding_namespace" {
//  value = kubernetes_service_account.tiller.metadata[0].namespace
  value = kubernetes_cluster_role_binding.tiller.subject[0].namespace
}
