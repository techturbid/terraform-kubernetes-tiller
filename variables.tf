variable "kubernetes_service_account_name" {
  default = "tiller"
  description = "Tiller's service account name"
}
variable "kubernetes_service_account_namespace" {
  default = "kube-system"
  description = "Namespace where Tiller will be installed"
}
variable "wait_for_resources" {}
