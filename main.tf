resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}

resource "kubernetes_service_account" "tiller" {
  depends_on = ["null_resource.wait_for_resources"]
  metadata {
    name      = var.kubernetes_service_account_name
    namespace = var.kubernetes_service_account_namespace
  }

  automount_service_account_token = true
}

resource "kubernetes_cluster_role_binding" "tiller" {
  depends_on = ["kubernetes_service_account.tiller"]
  metadata {
    name = kubernetes_service_account.tiller.metadata[0].name
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.tiller.metadata[0].name
    namespace = kubernetes_service_account.tiller.metadata[0].namespace
  }
}
