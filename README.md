Techturbid Terraform Registry - tiller
---
#### Tiller module used by techturbid's GKE modules like gke_base

By default this module will create a kubernetes_service_account and a kubernetes_cluster_role_binding for `Tiller`
 
It will mirror the cluster-admin credentials thus giving cluster wide admin access for `Tiller`

--- 
 
The variables with default values are:

|             **Variable**           |       **Value**       |
|------------------------------------|:----------------------|
| kubernetes_service_account_name          | tiller      |
| kubernetes_service_account_namespace     | kube-system          |

---

This module will output the following value:

|              **Output**            |                       **Value**               |
|------------------------------------|:----------------------------------------------|
| tiller_rolebinding_name                   | kubernetes_cluster_role_binding.tiller.subject[0].name              |
| tiller_rolebinding_namespace                   | kubernetes_cluster_role_binding.tiller.subject[0].namespace               |

---

This module requires the following providers:
```hcl-terraform
provider "kubernetes" {
  load_config_file = false
  host = "https://${module.gke.cluster_endpoint}"
  cluster_ca_certificate = base64decode(module.gke.cluster_ca_certificate)
  token = data.google_client_config.google-beta_current.access_token
}
```

---